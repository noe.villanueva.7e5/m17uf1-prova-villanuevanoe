using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    //public GameObject alien1;
    //public GameObject alien2;
    public float minRandom, maxRandom;
    private float nextActionTime = 0;
    public float period = 0.5f;
    public int randomAlien;
  
    public GameObject[] aliens;
    // Start is called before the first frame update
    void Start()
    {
        minRandom = transform.position.x - 10f;
        maxRandom = transform.position.x + 10f;
        
    }

    // Update is called once per frame
    void Update()
    {
        nextActionTime += Time.deltaTime;
        if (nextActionTime > period)
        {
            nextActionTime = 0;
            SpawnAlien();
        }
    }

    public void SpawnAlien()
    {
        Instantiate(aliens[Random.Range(0,2)], new Vector3(Random.Range(minRandom, maxRandom), transform.position.y, 0), Quaternion.identity);
    }

}
