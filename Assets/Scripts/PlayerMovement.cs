using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private SpriteRenderer sprite;
    private Transform movement;
    private DataPlayer Player;

    // Start is called before the first frame update
    void Start()
    {
        movement = gameObject.GetComponent<Transform>();
        Player = gameObject.GetComponent<DataPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        transform.position = transform.position + new Vector3(horizontalInput * Player.Speed * Time.deltaTime, verticalInput * Player.Speed * Time.deltaTime, 0);
    
        if (Mathf.Abs(transform.position.x) > 10)
        {
           
        }
           
    }


}
