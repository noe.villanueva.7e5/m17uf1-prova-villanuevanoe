using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private DataEnemy Enemy;
    private void Awake()
    {
       
    }
    // Start is called before the first frame update
    void Start()
    {
        Enemy = gameObject.GetComponent<DataEnemy>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0 , transform.position.y * Enemy.Speed * Time.deltaTime, 0);
        Debug.Log(transform.position.y * Enemy.Speed * Time.deltaTime);
    }
}
