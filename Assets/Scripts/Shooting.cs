using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject Bullet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("space"))
        {
            SpawnBullet();
        }
        Bullet.transform.position = transform.position + new Vector3(0, transform.position.y * 5f * Time.deltaTime, 0);
        //Bullet.transform.position += new Vector3(transform.position.x , transform.position.y * Time.deltaTime, 0);
    }

    public void SpawnBullet()
    {
        Instantiate(Bullet, new Vector3(transform.position.x, transform.position.y * Time.deltaTime, 0), Quaternion.identity);
    }
}
